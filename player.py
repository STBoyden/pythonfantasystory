"""
File: player.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: Contains the player characteristics for the game
"""

import enemies as en
from game_setup import game_setup


class player(object):
    name = game_setup.player_name
    health = 150
    base_damage = 10
    speed = 5

    def print_player_stats(self):
        stats_line = "Name: {0}; Health: {1}; Base Damage: {2}; Damage Mult: {3}; Speed: {4}".format(
            self.name, self.health, self.base_damage, game_setup.player_damage_multiplier, self.speed)
        return stats_line

    def update_base_damage(self, damage_mult=1):
        self.base_damage = 10 * damage_mult

    def update_player_name(self, player_name=1):
        self.name = player_name
