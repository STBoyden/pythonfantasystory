#!/bin/bash

if ! [ $(id -u) = 0 ]; then
  echo "Please run as root"
  exit 1
fi

INSTALL_DIR="/usr/bin/python-fantasy-game-files"

if [ -d "$INSTALL_DIR" ]; then
  echo "$INSTALL_DIR found: deleting..."
  rm -rf "$INSTALL_DIR"
fi

echo ""
echo "Copying \"run.sh\" to \"/usr/bin/\" as \"python-fantasy-game\""
cp run.sh "/usr/bin/python-fantasy-game"
echo "Creating install directory at \"$INSTALL_DIR/\"..."
mkdir "$INSTALL_DIR/"
echo "Copying files to install directory..."
cp -r * "$INSTALL_DIR/"

echo ""
echo "Done!"
echo "Restart your shell for changes to take effect"
