# Python Fantasy Story
A command line fantasy story written in Python!

## How to Download, Install and Play
### Linux/Mac OSX
- To download, enter in a terminal: `$ git clone https://bitbucket.org/STBoyden/PythonFantasyStory`
- To install:
    - Navigate into the cloned repository with `$ cd PythonFantasyStory`
    - Enter in terminal: `# ./install.sh`
- To run:
    - If in the same directory as the game: `$ ./run.sh` or `$ python main.py`
    - If not in the same directory as the game: `$ python-fantasy-game`

### Windows
- To download, enter in powershell or command prompt: `git clone
  https://bitbucket.org/STBoyden/PythonFantasyStory`
- To install:
  - Navigate into the cloned repository with `cd PythonFantasyStory`
  - Enter in powershell or command prompt as administrator: `install.bat`
- To run:
  - If in the same directory as the game: `run.bat` or `python main.py`
  - If not in the same directory as the game: `python-fantasy-game`

### Requirements
- [Latest terminaltables version](https://robpol86.github.io/terminaltables):
  + Install with `pip install terminaltables` (`sudo`/admin may be required)
- [Latest colorama version](https://pypi.python.org/pypi/colorama)
  + Install with `pip install colorama` (`sudo`/admin may be required)
- [Latest termcolor version](https://pypi.python.org/pypi/termcolor)
  + Install with `pip install termcolor` (`sudo`/admin may be required)


## Repository Website
The website can be located [here](https://stboyden.github.io/PythonFantasyStory/).
