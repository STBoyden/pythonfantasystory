@echo off
goto check_perms

:check_perms
  net session >nul 2>&1

  if %errorLevel% == 0 (
    goto install
  ) else (
    echo "Invalid permissions: run as admin"
    exit
  )

:check_folder
  if not exist "%windir%\system32\python-fantasy-game-files\" (
    echo "No installation directory found"
    exit
  ) else (
    goto uninstall
  )

:uninstall
  echo "Deleting game executable..."
  del %windir%\system32\python-fantasy-game.bat
  echo "Removing game files..."
  rmdir /S /Q %windir%\system32\python-fantasy-game-files\
  echo.
  echo "Finished uninstallation"
  echo "Restart your command prompt or powershell for changes to take effect"
  exit
