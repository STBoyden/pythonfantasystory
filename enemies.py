"""
File: enemies.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: Contains the enemy classes for the game
"""

from game_setup import game_setup


class enemies(object):
    ###
    #   Description
    # The class where all the enemy subclasses derive
    ###
    name = ""
    health = 100
    base_damage = 0
    def_base_damage = 0
    speed = 5

    def get_enemy_name(self):
        return self.name

    def update_base_damages(self, damage_mult=1):
        ###
        #   Description
        # Updates all the base damages for the subclasses of the Enemies class
        ###
        for sub in enemies.__subclasses__():
            sub.base_damage = sub.def_base_damage * damage_mult


class orc(enemies):             # main.py: enemy_classes[0]
    ###
    #    Description
    # The orc enemy subclass
    ###
    def __init__(self):
        enemies.__init__(self)

    name = "Orc"
    health = 100
    def_base_damage = 12
    base_damage = 12
    speed = 2.5


class werewolf(enemies):        # main.py: enemy_classes[1]
    ###
    #   Description
    # The werewolf enemy subclass
    ###
    def __init__(self):
        enemies.__init__(self)

    name = "Werewolf"
    health = 150
    damage = 20
    def_base_damage = 20
    speed = 7

# TODO: Add more enemies
