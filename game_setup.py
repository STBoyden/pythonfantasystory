"""
File: game_setup.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: Sets up the game
"""

from datetime import datetime

from termcolor import colored, cprint
from terminaltables import DoubleTable, SingleTable


class game_setup(object):
    player_damage_multiplier = 0
    enemy_damage_multiplier = 0
    player_name = "None set"
    game_difficulty = "None set"

    def get_time(self):
        return str(datetime.now().strftime('%d/%m/%y %H:%M'))

    def set_player_name(self):
        print("Set player name")
        while True:
            self.player_name = input("{0}".format(
                colored(self.get_time() + " | name > ", "white", attrs=["bold"])))
            print("Confirm name: \"{0}\"?".format(
                colored(self.player_name, "white", attrs=["bold"])))
            confirm_name = ""
            confirm_name = input("{0}".format(
                colored(self.get_time() + " | confirm name (y/n) > ", "white", attrs=["bold"])))
            confirm_name = confirm_name.lower()
            while True:
                if confirm_name == "y":
                    if self.player_name != "":
                        return 0
                    else:
                        # print("{0}Name needs a valid value{1}".format(textdecorations.blink + colours.red, textdecorations.clear))
                        cprint("Name needs a valid value",
                               "red", attrs=["reverse"])
                        break
                elif confirm_name == "n":
                    print("Restarting name setting...")
                    break
                else:
                    cprint("Invalid input", "red", attrs=["reverse"])
                    break

    def set_difficulty(self):
        print("Set difficulty (input \"difs\" to print a list of valid difficulties)")
        while True:
            inp = input("{0}".format(colored(self.get_time() +
                                             " | difficulty > ", "white", attrs=["bold"])))
            inp = inp.lower()
            if inp == "difs":
                data = [
                    [colored("Input", "white", attrs=["bold"]),
                     colored("Output", "white", attrs=["bold"])],
                    ["veasy",
                        "Very Easy (x1.5 to enemies, x0.25 from enemies)"],
                    ["easy", "Easy (x1.25 to enemies, x0.5 from enemies)"],
                    ["normal", "Normal (x1 to enemies, x1 from enemies)"],
                    ["hard", "Hard (x0.75 to enemies, x1.25 from enemies)"],
                    ["vhard",
                        "Very Hard (x0.5 to enemies, x1.5 from enemies)"],
                    ["impossible",
                        "Impossible (x0.25 to enemies, x1.75 from enemies)"]
                ]
                table = SingleTable(data, "{0}".format(
                    colored("Difficulties", "white", attrs=["bold"])))
                print(table.table)
            elif inp == "veasy":
                self.player_damage_multiplier = 1.5
                self.enemy_damage_multiplier = 0.25
                self.game_difficulty = "Very Easy"
                break
            elif inp == "easy":
                self.player_damage_multiplier = 1.25
                self.enemy_damage_multiplier = 0.5
                self.game_difficulty = "Easy"
                break
            elif inp == "normal":
                self.player_damage_multiplier = 1
                self.enemy_damage_multiplier = 1
                self.game_difficulty = "Normal"
                break
            elif inp == "hard":
                self.player_damage_multiplier = 0.75
                self.enemy_damage_multiplier = 1.25
                self.game_difficulty = "Hard"
                break
            elif inp == "vhard":
                self.player_damage_multiplier = 0.5
                self.enemy_damage_multiplier = 1.5
                self.game_difficulty = "Very Hard"
                break
            elif inp == "impossible":
                self.player_damage_multiplier = 0.25
                self.enemy_damage_multiplier = 1.75
                self.game_difficulty = "Impossible"
                break
            else:
                cprint("Invalid difficulty", "red", attrs=["reverse"])

    def setup(self):
        print("Starting setup..")
        self.set_player_name()
        self.set_difficulty()
        print("Setup finished")
