#!/bin/bash

if ! [ $(id -u) = 0 ]; then
  echo "Please run as root"
  exit 1
fi

INSTALL_DIR="/usr/bin/python-fantasy-game-files"
USR_BIN="/usr/bin"

if ! [ -d "$INSTALL_DIR/" ]; then
  echo "No installation directory found"
  exit 1
fi

echo "Deleting game executable..."
rm "$USR_BIN/python-fantasy-game"
echo "Removing game files..."
rm -rf "$INSTALL_DIR/"

echo ""
echo "Finished uninstallation"
echo "Restart your shell for changes to take effect"
