"""
File: story.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: Contains the story 'acts' for the game
"""

from datetime import datetime

from termcolor import colored, cprint
from terminaltables import DoubleTable, SingleTable

from elements import story_elements
from enemies import enemies
from game_setup import game_setup
from player import player


def print_interface():
    data = [
        [colored("Statistic", "white", attrs=["bold"]),
         colored("Value", "white", attrs=["bold"])],
        ["Name",                "{0}".format(player.name)],
        ["Health",              "{0}HP".format(player.health)],
        ["Base damage",         "{0}".format(player.base_damage)],
        ["Speed",               "{0}".format(player.speed)],
        ["Damage multiplier",   "{0}".format(
            game_setup.player_damage_multiplier)]
    ]
    table = SingleTable(data, "{0}".format(
        colored("Interface", "white", attrs=["bold"])))
    print(table.table)


class gameacts(object):
    def start_game(self):
        print("Starting game...")
        act_one.scene_one(act_one)

    def getact(self):
        return __class__.__name__


class act_one(gameacts):
    def getact(self):
        return __class__.__name__

    def scene_one(self):
        print(self.getact(self))
        print_interface()

    def scene_two(self):
        print("")


class act_two(gameacts):
    def getact(self):
        return __class__.__name__

    def scene_one(self):
        print("")

    def scene_two(self):
        print("")
