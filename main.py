"""
File: main.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: The main Python file for the game
"""

import os
import random
from datetime import datetime

import colorama
from termcolor import colored, cprint
from terminaltables import DoubleTable, SingleTable

from elements import story_elements as se
from enemies import *
from game_setup import game_setup
from player import player
from story import gameacts

colorama.init()


def print_help():
    data = [
        [colored("Input", "white", attrs=["bold"]),
         colored("Output", "white", attrs=["bold"])],
        ["help", "Prints this menu"],
        ["quit", "Quits the program"],
        ["setup", "Starts the setup process of the game"],
        ["start", "Starts the game (setup must have been run)"],
        ["name", "Prints the currently set name"],
        ["difficulty", "Prints the currently set difficulty"]
    ]
    table = SingleTable(data, "{0}".format(
        colored("Help", "white", attrs=["bold"])))
    print(table.table)


def get_time():
    return str(datetime.now().strftime('%d/%m/%y %H:%M'))


def main():
    gs = game_setup()
    pl = player()
    en = enemies()
    ga = gameacts()
    # a list with all the enemy subclasses
    enemy_classes = enemies.__subclasses__()
    gacts = gameacts.__subclasses__()

    # for i in range(0, len(enemy_classes)):
    #     print("Enemy name: {0}".format(enemy_classes[i].name))

    print("Welcome to the Fantasy Story Game in Python!")
    print("Input \"help\" for help!\n")
    been_setup = False
    en.update_base_damages()
    while True:
        inp = input("{0}".format(
            colored(get_time() + " | menu > ", "white", attrs=["bold"])))
        inp = inp.lower()
        if inp == "help":
            print_help()
        elif inp == "quit":
            print("Quitting...")
            os._exit(1)
        elif inp == "name":
            print("Player name: {0}".format(gs.player_name))
        elif inp == "difficulty":
            print("Game difficulty: {0}".format(gs.game_difficulty))
        elif inp == "setup":
            gs.setup()
            been_setup = True
            pl.update_base_damage(damage_mult=gs.player_damage_multiplier)
            en.update_base_damages(damage_mult=gs.enemy_damage_multiplier)
            pl.update_player_name(player_name=gs.player_name)
        elif inp == "start":
            if not been_setup:
                print("Game has not been setup, please setup game by running \"setup\" ")
            else:
                ga.start_game()
        else:
            cprint("Invalid input", "red", attrs=["reverse"])


main()
