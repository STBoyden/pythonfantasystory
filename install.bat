@echo off
goto check_perms

:check_perms
  net session >nul 2>&1

  if %errorLevel% == 0 (
    goto install
  ) else (
    echo Invalid permissions: run as admin
    exit
  )

:install
  echo.
  echo "Copying 'run.bat' to %windir%\system32\ as python-fantasy-game.bat..."
  xcopy run.bat %windir%\system32\python-fantasy-game.bat
  echo "Creating installation directory..."
  mkdir %windir%\system32\python-fantasy-game-files\
  echo "Copying files to install directory..."
  xcopy /s . %windir%\system32\python-fantasy-game-files\
  echo.
  echo "Done!"
  echo "Restart your command prompt or powershell for changes to take effect"
  exit

