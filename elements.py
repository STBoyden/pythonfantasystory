"""
File: elements.py
Author: STBoyden
Email: STBoyden@gmail.com
Github: https://github.com/STBoyden
Description: Contains the various story elements
"""

from random import randint


class story_elements():
    external_environments = [
        ["Forest",  "forest"],
        ["Fields",  "fields"],
        ["Desert",  "desert"],
        ["Tundra",  "tundra"]
    ]

    internal_environments = [
        ["Room",    "room"],
        ["Attic",   "attic"],
        ["Chamber", "chamber"]
    ]

    structures = [
        ["Cave",    "cave"],
        ["House",   "house"],
        ["Mansion", "mansion"]
    ]

    npc_names = [
        "Yuthgard",
        "Krech",
        "Jorvar",
        "Balgar"
    ]

    external_env_sentence_prefix = [
        "You find yourself in the",
        "You find yourself in some",
        "You find yourself in a"
    ]

    internal_env_sentence_prefix = [
        "You look around the",
        "You look around a"
    ]

    env_adjectives = [
        "\b",
        "mystical",
        "evil",
        "shadowy",
        "mysterious",
        "magical",
        "strange"
    ]

    def get_random_external(self):
        prefix = self.external_env_sentence_prefix[randint(
            0, len(self.external_env_sentence_prefix)-1)]
        env_adjective = self.env_adjectives[randint(
            0, len(self.env_adjectives)-1)]
        ext_environment = self.external_environments[randint(
            0, len(self.external_environments)-1)][1]
        line = "{0} {1} {2}".format(prefix, env_adjective, ext_environment)
        return line

    def get_random_internal(self):
        prefix = self.internal_env_sentence_prefix[randint(
            0, len(self.internal_env_sentence_prefix)-1)]
        env_adjective = self.env_adjectives[randint(
            0, len(self.env_adjectives)-1)]
        int_environment = self.internal_environments[randint(
            0, len(self.internal_environments)-1)][1]
        line = "{0} {1} {2}".format(prefix, env_adjective, int_environment)
        return line
